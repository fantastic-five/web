import React, { Component } from 'react';
import ReactDOM from 'react-dom';

export default class ReportsMap extends Component {
  componentDidUpdate(prevProps, prevState) {
    if (
      prevProps.google !== this.props.google ||
      prevProps.reports !== this.props.reports
    ) {
      this.loadMap();
    }
  }

  loadMap() {
    if (this.props && this.props.google) {
      const { google } = this.props;
      const maps = google.maps;

      const mapRef = this.refs.map;
      const node = ReactDOM.findDOMNode(mapRef);

      const mapConfig = Object.assign(
        {},
        {
          center: { lat: 45.763127, lng: 21.219292 },
          zoom: 12,
          gestureHandling: 'cooperative',
          mapTypeId: 'terrain'
        }
      );
      this.map = new maps.Map(node, mapConfig);
      var heatmapData = [];

      const infowindow = new google.maps.InfoWindow();

      this.props.reports.forEach(report => {
        heatmapData.push({
          location: new google.maps.LatLng(
            report.coords.lat,
            report.coords.long
          ),
          weight: report.mag || 5
        });

        const marker = new google.maps.Marker({
          position: {
            lat: report.coords.lat,
            lng: report.coords.long
          },
          map: this.map,
          title: report.category,
          opacity: 0.2
        });

        marker.addListener('click', function() {
          infowindow.setContent(
            `<h3>${report.category}</h3><img src="${report.img}">`
          );
          infowindow.open(this.map, marker);

          var element = document.getElementById(report.id);
          element.scrollIntoView({
            behavior: 'smooth',
            block: 'center',
            inline: 'nearest'
          });
        });
      });

      const heatmap = new google.maps.visualization.HeatmapLayer({
        dissipating: true,
        data: heatmapData,
        gradient: ['transparent', 'aqua', 'blue', 'red'],
        radius: 60
      });
      heatmap.setMap(this.map);
    }
  }

  render() {
    const style = {
      width: '100%',
      height: '100vh'
    };

    return (
      <div ref="map" style={style}>
        loading map...
      </div>
    );
  }
}
