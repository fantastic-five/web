import React, { Component } from 'react';
import ReportsMap from './components/ReportsMap';
import { GoogleApiWrapper } from 'google-maps-react';
import './App.css';

class App extends Component {
  state = {
    categories: [],
    reports: [],
    filteredReports: [],
    hasQuery: true
  };

  componentDidMount = async () => {
    let response = await fetch(
      'https://shamyamy-api.herokuapp.com/api/getAllPoints'
    );
    let reps = await response.json();

    reps.categories = reps.categories.map(cat => ({
      name: cat,
      checked: true
    }));

    reps.reports.forEach(rep => {
      const cat = rep.category === 'ambrosia' ? 'plants' : rep.category;

      rep.img =
        rep.thumbnailImg ||
        `https://loremflickr.com/160/120/${cat}?no-cache=${Math.random(100)}`;
    });

    reps.filteredReports = [...reps.reports];
    reps.hasQuery = window.location.search.slice(1).length > 0;

    this.setState(reps);
  };

  filter = () => {
    const activeCategories = this.state.categories
      .filter(cat => cat.checked)
      .map(el => el.name);

    const filtered = this.state.reports.filter(rep => {
      return activeCategories.includes(rep.category);
    });

    this.setState({ filteredReports: filtered });
  };

  filterItems = () => {
    const categories = this.state.categories;

    return categories.map(cat => (
      <label key={cat.name}>
        <input
          type="checkbox"
          checked={cat.checked}
          onChange={() => {
            categories.forEach(categ => {
              if (categ.name === cat.name) {
                categ.checked = !categ.checked;
              }
            });

            this.setState({ categories });
            this.filter();
          }}
        />
        {cat.name}
      </label>
    ));
  };

  listItems = () =>
    this.state.filteredReports.map(report => (
      <li key={report.id} id={report.id}>
        <img className="responsiveImg" src={report.img} alt={report.name} />

        <p>
          <strong>Category: </strong>
          {report.category || 'Placeholder Category'}
        </p>
      </li>
    ));

  render() {
    return (
      <div className="container">
        <div className={!this.state.hasQuery ? 'sidebar show' : 'sidebar'}>
          <h1>Shamy Amy</h1>
          <div>
            <h2>Filters:</h2>
            <ul>{this.filterItems()}</ul>
            <h2>Reports:</h2>
          </div>
          <div className="list">
            <ul>{this.listItems()}</ul>
          </div>
        </div>
        <main className="main">
          <ReportsMap
            google={this.props.google}
            reports={this.state.filteredReports}
            {...this.props}
          />
        </main>
      </div>
    );
  }
}

export default GoogleApiWrapper({
  apiKey: 'AIzaSyChZPizXo_3sk70Cm4yveOd0YfQtuxc7As',
  libraries: ['visualization']
})(App);
